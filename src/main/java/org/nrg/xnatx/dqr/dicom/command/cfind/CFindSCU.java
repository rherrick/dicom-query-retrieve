/*
 * dicom-query-retrieve: org.nrg.xnatx.dqr.dicom.command.cfind.CFindSCU
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.dqr.dicom.command.cfind;

import org.nrg.xnatx.dqr.domain.Patient;
import org.nrg.xnatx.dqr.domain.Series;
import org.nrg.xnatx.dqr.domain.Study;
import org.nrg.xnatx.dqr.dto.PacsSearchCriteria;
import org.nrg.xnatx.dqr.dto.PacsSearchResults;

import java.util.Optional;

public interface CFindSCU {

    PacsSearchResults<Patient> cfindPatientsByExample(final PacsSearchCriteria searchCriteria);

    Optional<Patient> cfindPatientById(final String patientId);

    PacsSearchResults<Study> cfindStudiesByExample(final PacsSearchCriteria searchCriteria);

    Optional<Study> cfindStudyById(final String studyInstanceUid);

    PacsSearchResults<Series> cfindSeriesByStudy(final Study Study);

    PacsSearchResults<Series> cfindSeriesByStudyUid(final String studyUid);

    Optional<Series> cfindSeriesById(final String seriesInstanceUid);
}
