/*
 * dicom-query-retrieve: org.nrg.xnatx.dqr.services.ExecutedPacsRequestService
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.dqr.services;

import org.nrg.xnatx.dqr.domain.entities.ExecutedPacsRequest;

/**
 * Created by mike on 1/19/18.
 */
public interface ExecutedPacsRequestService extends BasePacsRequestService<ExecutedPacsRequest> {
    String TYPE = "executed";
}
