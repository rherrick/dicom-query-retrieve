/*
 * dicom-query-retrieve: org.nrg.xnatx.dqr.domain.DqrDomainObject
 * XNAT http://www.xnat.org
 * Copyright (c) 2005-2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.dqr.domain;

public interface DqrDomainObject {
    String getUniqueIdentifier();
}
