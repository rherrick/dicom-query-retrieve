#* @vtlvariable name="link" type="org.apache.turbine.services.pull.tools.TemplateLink" *#
#* @vtlvariable name="turbineUtils" type="org.nrg.xdat.turbine.utils.TurbineUtils" *#
#* @vtlvariable name="pacsList" type="java.util.List" *#
#* @vtlvariable name="pacs" type="org.nrg.xnatx.dqr.domain.entities.Pacs" *#
#* @vtlvariable name="projectId" type="java.lang.String" *#
#* @vtlvariable name="displayManager" type="org.nrg.xdat.display.DisplayManager" *#
#* @vtlvariable name="content" type="org.apache.turbine.services.pull.tools.ContentTool" *#
<!-- BEGIN templates/screens/PacsSessionFinder.vm -->

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="$content.getURI("style/dqr/dqr.css")">

<script type="text/javascript" src="$content.getURI("scripts/dqr/PacsSessionFinder.js")"></script>
<script type="text/javascript" src="$content.getURI("scripts/dqr/PacsSeriesFinder.js")"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.9.2/jquery-ui.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="$content.getURI("scripts/dqr/datejs.js")"></script>
<script type="text/javascript" src="$content.getURI("scripts/dqr/StudyDatePicker.js")"></script>
<script type="text/javascript" src="$content.getURI("scripts/xnat/plugin/dqr/CustomDataTablesFilters.js")"></script>

<script type="text/javascript">
    var terms = {
        termForPatient: "${displayManager.getSingularDisplayNameForSubject()}",
        termForStudy: "${displayManager.getSingularDisplayNameForImageSession()}"
    }

    jq(document).ready(function () {
        if (window.pacsSessionFinder === undefined) {
            window.pacsSessionFinder = new PacsSessionFinder(
                    "sessionSearchForm", "sessionSelectionForm", "studyInstanceUid", "hiddenPacsId", "pacsSessionFinderDiv", terms);
        }

        jq("#searchButton").click(function (event) {
            event.preventDefault();
            if (jq("#sessionSearchForm").validate()) {
                window.studyDatePicker.formatDateFieldsInPlace();
                window.pacsSessionFinder.findSessions(jq("#pacsId").val());
            }
        });

        jq("#sessionSearchForm input[type=text],select").keydown(function (event) {
            var enterKeyCode = 13;
            if (event.keyCode === enterKeyCode) {
                jq("#searchButton").click();
            }
        });

        if (window.studyDatePicker === undefined) {
            window.studyDatePicker = new StudyDatePicker("studyDateFrom", "studyDateTo", "studyDateIsToday");
        }

        var searchValidationRules = window.studyDatePicker.getStudyDateValidationRules();
        searchValidationRules["pacsId"] = {
            required: true
        };

        jq("#sessionSearchForm").validate({
            rules: searchValidationRules,
            groups: window.studyDatePicker.getStudyDateValidationGroup(),
            errorPlacement: function (error, element) {
                if (jq(element).prop("id") === "studyDateFrom" || jq(element).prop("id") === "studyDateTo") {
                    jq("#studyDateValidationErrorMessageHolder").append(error);
                } else {
                    error.css("color", "red");
                    error.insertAfter(element);
                }
            },
            messages: {
                pacsId: "Please choose a DICOM AE to query."
            }
        });

        if ((1 + 1) === jq("#pacsId option").length) {
            jq("#pacsId option").last().prop("selected", "true");
        }

        jq("#pacsId").focus();
    });
</script>

<h2 class="edit_title">Import ${displayManager.getSingularDisplayNameForImageSession()} From PACS</h2>
<p><strong>Step 1: Find ${displayManager.getSingularDisplayNameForSubject()} ${displayManager.getPluralDisplayNameForImageSession()}</strong></p>
<form id="sessionSearchForm" class="friendlyForm mini">
    <div id="searchFormControls">
        <!--  left column -->
        <div style="display: inline-block; vertical-align: top; width: 30%; margin-right: 8%">
            <h4>Configure Search (Required)</h4>
            <p>
                <label for="pacsId">Select DICOM AE to Query</label>
                <select id="pacsId" name="pacsId">
                    <option VALUE="">(SELECT)</option>
                    #foreach($pacs in $pacsList)
                        <option VALUE="$pacs.id" #if($pacs.isDefaultQueryRetrievePacs()) selected="selected" #end>#if(!${turbineUtils.isBlankString($pacs.label)})${pacs.aeTitle} #else ${pacs.label} #end</option>
                    #end
                </select>
            </p>
        </div>

        <div style="display: inline-block; vertical-align: top; width: 60%">
            <h4>Search Terms</h4>
            <p>
                <label for="accessionNumber">Accession Number</label>
                <input id="accessionNumber" name="accessionNumber" type="text" maxlength="50"/>
            </p>
            <div style="display:none; background-color: #e4efff; margin-left: -2px; padding: 6px 2px 2px;" id="advFields"> <!-- advanced fields -->
                <p>
                    <label for="patientId">${displayManager.getSingularDisplayNameForSubject()} ID</label>
                    <input id="patientId" name="patientId" type="text" maxlength="50"/>
                </p>
                <p>
                    <label for="patientName">${displayManager.getSingularDisplayNameForSubject()} Name</label>
                    <input id="patientName" name="patientName" type="text" maxlength="50"/>
                </p>
                <div style="line-height: 1">
                    <label for="studyDateFrom">${displayManager.getSingularDisplayNameForImageSession()} Date Range</label>
                    <span style="margin:0 6px 0 0;">From</span><input id="studyDateFrom" name="studyDateFrom" type="text" maxlength="10" style="width: 90px;"/>
                    <span style="margin:0 6px;">To</span><input id="studyDateTo" name="studyDateTo" type="text" maxlength="10" style="width: 90px;"/>
                </div>
                <p>
                    <label>&nbsp;</label>
                    <input type="checkbox" id="studyDateIsToday" name="studyDateIsToday"/><em>The ${displayManager.getSingularDisplayNameForImageSession()} happened today</em>
                </p>
                <span id="studyDateValidationErrorMessageHolder" style="color:red;"/>
            </div>
            <p><em><a href="javascript:" onclick="$('#advFields').toggle()">Toggle advanced search fields</a></em></p>

            <p>
                <button id="searchButton" class="btn1">Search</button>
            </p>
        </div>
    </div>

    <div class="clearfix clear"></div>
</form>


<form id="sessionSelectionForm" method="post" action="$link.setAction("ChoosePacsSession").addPathInfo("project",${projectId})" style="display:none;">
    <input type="hidden" name="project" id="project" value="${projectId}"/>
    <input type="hidden" id="studyInstanceUid" name="studyInstanceUid"/>
    <input type="hidden" id="hiddenPacsId" name="pacsId"/>
</form>

<div id="pacsSessionFinderDiv" style="margin: 1em 0"/>

<!-- END templates/screens/PacsSessionFinder.vm -->
